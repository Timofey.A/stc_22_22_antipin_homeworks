package ru.inno.cars.repository;

import ru.inno.cars.models.Car;

import java.util.List;

public interface CarRepository {
    List<Car> read();

    void write(Car car);

}
