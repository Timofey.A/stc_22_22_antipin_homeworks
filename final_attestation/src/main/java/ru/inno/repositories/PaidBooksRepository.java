package ru.inno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.models.FreeBook;
import ru.inno.models.PaidBook;

import java.util.List;

public interface PaidBooksRepository extends JpaRepository<PaidBook, Long> {

    List<PaidBook> findAllByStateNot(PaidBook.State deleted);
}
