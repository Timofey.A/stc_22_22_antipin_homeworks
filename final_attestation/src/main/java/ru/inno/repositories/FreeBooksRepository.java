package ru.inno.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.models.FreeBook;

import java.util.List;

public interface FreeBooksRepository extends JpaRepository<FreeBook, Long> {
    List<FreeBook> findAllByStateNot(FreeBook.State deleted);
}
