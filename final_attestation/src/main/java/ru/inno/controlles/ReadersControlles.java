package ru.inno.controlles;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.models.Reader;

import ru.inno.services.ReadersServices;

@RequiredArgsConstructor
@Controller
public class ReadersControlles {

    private final ReadersServices readersServices;

    @GetMapping("/readers")
    public String getReadersPage(Model model) {
        model.addAttribute("readers", readersServices.getAllReaders());
        return "Readers";
    }
    @PostMapping("/readers")
    public String addReader(Reader reader) {
        readersServices.addReader(reader);
        return "redirect:/readers";
    }
    @GetMapping("/readers/{reader-id}")
    public String getReaderPage(@PathVariable("reader-id") Long id, Model model) {
        model.addAttribute("reader", readersServices.getReader(id));
        return "Reader";
    }

    @PostMapping("/readers/{reader-id}/update")
    public String updateReader(@PathVariable("reader-id") Long readerId, Reader reader) {
        readersServices.updateReader(readerId, reader);
        return "redirect:/readers/" + readerId;
    }

    @GetMapping("/readers/{reader-id}/delete")
    public String updateReader(@PathVariable("reader-id") Long readerId) {
        readersServices.deleteReader(readerId);
        return "redirect:/readers";
    }
}
