package ru.inno.controlles;

import lombok.RequiredArgsConstructor;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.models.FreeBook;
import ru.inno.models.PaidBook;

import ru.inno.security.details.CastomReaderDetails;
import ru.inno.services.PaidBooksServices;

@RequiredArgsConstructor
@Controller
public class PaidBookControlles {
    private final PaidBooksServices paidBooksServices;

    @GetMapping("/paidbooks")
    public String getPaidBooksPage(@AuthenticationPrincipal CastomReaderDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getReader().getRole());
       model.addAttribute("paidbooks", paidBooksServices.getAllPaidBooks());
        return "PaidBooks";
    }
    @PostMapping("/paidbooks")
    public String addPaidBook(PaidBook paidBook) {
        paidBooksServices.addPaidBook(paidBook);
        return "redirect:/paidbooks";
    }
    @GetMapping("/paidbooks/{paidbook-id}")
    public String getPaidBookPage(@PathVariable("paidbook-id") Long id, Model model) {
        model.addAttribute("paidbook", paidBooksServices.getPaidBook(id));
        return "PaidBook";
    }

    @PostMapping("/paidbooks/{paidbooks-id}/update")
    public String updatePaidBook(@PathVariable("paidbooks-id") Long paidBookId,PaidBook paidBook) {
        paidBooksServices.updatePaidBook(paidBookId, paidBook);
        return "redirect:/paidbooks/" + paidBookId;
    }

    @GetMapping("/paidbooks/{paidbooks-id}/delete")
    public String updateFreeBook(@PathVariable("paidbooks-id") Long paidBookId) {
        paidBooksServices.deletePaidBook(paidBookId);
        return "redirect:/paidbooks";
    }

}
