package ru.inno.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import ru.inno.security.details.CastomReaderDetailsServices;

@EnableWebSecurity
public class SecurityConfig {
    @Autowired
    private UserDetailsService castomReaderDetailsServices;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();
        httpSecurity.formLogin()
                .loginPage("/signIn")
                .defaultSuccessUrl("/profile")
                .failureUrl("/signIn?error")
                .usernameParameter("email")
                .passwordParameter("password")
                .permitAll();


       httpSecurity.authorizeHttpRequests().antMatchers("/signIn/**").permitAll();
       httpSecurity.authorizeHttpRequests().antMatchers("/paidbooks/**").authenticated();
       httpSecurity.authorizeHttpRequests().antMatchers("/profile/**").authenticated();
       httpSecurity.authorizeHttpRequests().antMatchers("/freebooks/**").permitAll();
       httpSecurity.authorizeHttpRequests().antMatchers("/readers/**").hasAuthority("ADMIN");

        return httpSecurity.build();
    }
    @Autowired
    public void bindUserDetailsService(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(castomReaderDetailsServices).passwordEncoder(new BCryptPasswordEncoder());
    }
}
