package ru.inno.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.inno.models.Reader;
import ru.inno.repositories.ReadersRepository;
@RequiredArgsConstructor
@Service
public class CastomReaderDetailsServices implements UserDetailsService {
    private final ReadersRepository readersRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Reader reader = readersRepository.findByEmail(email).orElseThrow(() ->new UsernameNotFoundException("User not found"));
        CastomReaderDetails readerDetails = new CastomReaderDetails(reader);
        return readerDetails;
    }
}
