package ru.inno.security.details;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.inno.models.Reader;

import java.util.Collection;
import java.util.Collections;

public class CastomReaderDetails implements UserDetails {
    private Reader reader;

    public CastomReaderDetails(Reader reader) {
        this.reader = reader;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        String role = reader.getRole().toString();
        return Collections.singleton(new SimpleGrantedAuthority(role));
    }

    @Override
    public String getPassword() {
        return reader.getPassword();
    }

    @Override
    public String getUsername() {
        return reader.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !reader.getState().equals(Reader.State.DELETED);
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return reader.getState().equals(Reader.State.CONFIRMED);
    }

    public Reader getReader() {
        return reader;
    }
}
