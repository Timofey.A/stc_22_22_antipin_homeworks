package ru.inno.services.Impl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.models.Reader;
import ru.inno.repositories.ReadersRepository;
import ru.inno.services.ReadersServices;

import java.util.List;
@Service
@RequiredArgsConstructor
@Data
public class ReadersServicesImpl implements ReadersServices {
    private final ReadersRepository readersRepository;

    @Override
    public List<Reader> getAllReaders() {
        return readersRepository.findAllByStateNot(Reader.State.DELETED);
    }

    @Override
    public void addReader(Reader reader) {
        Reader newReader = Reader.builder()
                .email(reader.getEmail())
                .firstName(reader.getFirstName())
                .lastName(reader.getLastName())
                .state(Reader.State.NOT_CONFIRMED)
                .build();
        readersRepository.save(newReader);
    }

    @Override
    public Reader getReader(Long id) {
        return readersRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateReader(Long accountId, Reader updateData) {
        Reader readerForUpdate = readersRepository.findById(accountId).orElseThrow();
        readerForUpdate.setFirstName(updateData.getFirstName());
        readerForUpdate.setLastName(updateData.getLastName());
        readerForUpdate.save(readerForUpdate);
    }

    @Override
    public void deleteReader(Long accountId) {
        Reader readerForDelete = readersRepository.findById(accountId).orElseThrow();
        readerForDelete.setState(Reader.State.DELETED);
        readersRepository.save(readerForDelete);
    }
}
