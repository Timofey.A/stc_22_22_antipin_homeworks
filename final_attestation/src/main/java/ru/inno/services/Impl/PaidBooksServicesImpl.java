package ru.inno.services.Impl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.models.FreeBook;
import ru.inno.models.PaidBook;
import ru.inno.repositories.PaidBooksRepository;
import ru.inno.services.PaidBooksServices;

import java.util.List;

@Service
@RequiredArgsConstructor
@Data
public class PaidBooksServicesImpl implements PaidBooksServices {
    private final PaidBooksRepository paidBooksRepository;
    @Override
    public List<PaidBook> getAllPaidBooks() {
        return paidBooksRepository.findAllByStateNot( PaidBook.State.DELETED);
    }

    @Override
    public PaidBook getPaidBook(Long id) {
        return paidBooksRepository.findById(id).orElseThrow();
    }

    @Override
    public void addPaidBook(PaidBook paidBook) {
        PaidBook newPaidBook = PaidBook.builder()
                .title(paidBook.getTitle())
                .description(paidBook.getDescription())
                .state(PaidBook.State.NOT_CONFIRMED)
                .build();
        paidBooksRepository.save(newPaidBook);


    }

    @Override
    public void updatePaidBook(Long paidBookId, PaidBook updateData) {
        PaidBook paidBookForUpdate = paidBooksRepository.findById(paidBookId).orElseThrow();
        paidBookForUpdate.setTitle(updateData.getTitle());
        paidBookForUpdate.setDescription(updateData.getDescription());
        paidBooksRepository.save(paidBookForUpdate);

    }

    @Override
    public void deletePaidBook(Long paidBookId) {
        PaidBook paidBookForDelete = paidBooksRepository.findById(paidBookId).orElseThrow();
        paidBookForDelete.setState(PaidBook.State.DELETED);
        paidBooksRepository.save(paidBookForDelete);

    }
}
