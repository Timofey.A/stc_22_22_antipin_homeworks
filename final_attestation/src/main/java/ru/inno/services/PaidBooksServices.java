package ru.inno.services;

import ru.inno.models.PaidBook;

import java.util.List;

public interface PaidBooksServices {

    List<PaidBook> getAllPaidBooks();

    PaidBook getPaidBook(Long id);

    void addPaidBook(PaidBook paidBook);

    void updatePaidBook(Long paidBookId, PaidBook paidBook);

    void deletePaidBook(Long paidBookId);

}
