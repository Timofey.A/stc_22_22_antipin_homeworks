package ru.inno.services;

import ru.inno.models.Reader;

import java.util.List;

public interface ReadersServices {
    List<Reader> getAllReaders();

    void addReader(Reader reader);

    Reader getReader(Long id);

    void updateReader(Long accountId, Reader reader);

    void deleteReader(Long accountId);
}
