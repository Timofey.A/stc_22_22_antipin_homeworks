package ru.inno.models;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
public class PaidBook {

        public enum State {
            NOT_CONFIRMED, CONFIRMED, DELETED
        }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @Column(length = 1000)
    private String description;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
