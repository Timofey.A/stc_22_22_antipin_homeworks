package ru.inno.models;



import lombok.*;
import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
/*@EqualsAndHashCode(exclude = {"courses"})
@ToString(exclude = {"courses"})*/
@Builder
@Entity
public class Reader {

    public void save(Reader readerForUpdate) {
    }

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED
    }
    public enum Role {
        ADMIN, USER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
