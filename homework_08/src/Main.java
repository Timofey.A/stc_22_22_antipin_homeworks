public class Main {
    public static void main(String[] args) {
        int[] array = {12, 62, 4, 2, 100, 40, 56};

        ArrayTask sumElement = (array1, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            System.out.println("sum of array elements  : \n " + sum);
            return sum;
        };

        ArrayTask calcOfSumMaxElementNumber = (array1, from, to) -> {
            int i = from, right = 0, left = 0, max = 0, localMax = max;
            while (i <= to) {
                left = max;
                max = right;
                right = array1[i++];
                if (right < max && max > left) {
                    System.out.println("local max: \n" + max);
                    localMax = max;
                }
            }
            int num = localMax;
            int sum = 0;
            while (num > 0) {
                sum = sum + num % 10;
                num = num / 10;
            }
            System.out.println("Sum Max Element Number : " + sum);
            return sum;
        };
        ArraysTasksResolver.resolveTask(array, sumElement, 3, 5);
        ArraysTasksResolver.resolveTask(array, calcOfSumMaxElementNumber, 3, 5);
    }
}
