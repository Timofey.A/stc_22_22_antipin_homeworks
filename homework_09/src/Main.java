

public class Main {

    public static void main(String[] args) {
        List<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(8);
        integerArrayList.add(10);
        integerArrayList.add(11);
        integerArrayList.add(13);
        integerArrayList.add(11);
        integerArrayList.add(15);
        integerArrayList.add(-5);

        System.out.println("\nArrayList");
        ArrayList.printCollection(integerArrayList);
        integerArrayList.removeAt(2);
        integerArrayList.remove(8);
        integerArrayList.remove(3);
        System.out.println("\nnewArrayList");
        ArrayList.printCollection(integerArrayList);
    }
}